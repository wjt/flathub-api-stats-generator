"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime

from . import BaseReport
from model import DownloadModel


# FIXME: this report is not of good quality, we need to limit to eg 10 apps
class DownloadsAppByDateReport(BaseReport):
    HAS_DATA_STRING = False  # TODO: string output
    HAS_DATA_ZIPPED = True
    MODEL = DownloadModel

    def __init__(self, model):
        BaseReport.__init__(self, model)

        # Pre read all the apps
        self.apps = []

        for _, data in self.model.downloads_by_date():
            for app in data["refs"]:
                if app not in self.apps:
                    self.apps.append(app)
    @property
    def axis(self):
        return ["Time", "Downloads"]

    def data_zipped(self):
        return [
            zip(
                *(
                    [date, sum(data["refs"][app][arch][0] for arch in data["refs"].get(app, []))]
                    for date, data in self.model.downloads_by_date()
                )
            ) for app in self.apps
        ]

    def data_zipped_kwargs(self):
        return [
            {
                "label": app,
                "x_date": True,
            } for app in self.apps
        ]

    @property
    def title(self):
        return (
            "Downloads by app %s over time - %s" % (
                self.model.api.NAME,
                datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S")
            )
        )
