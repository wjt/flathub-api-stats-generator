"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime

from . import BaseReport
from model import DownloadModel


class DownloadsFlatpakVersionByDateReport(BaseReport):
    HAS_DATA_STRING = False  # TODO: string output
    HAS_DATA_ZIPPED = True
    MODEL = DownloadModel

    def __init__(self, model):
        BaseReport.__init__(self, model)

        # Pre read all the versions
        self.versions = []

        for _, data in self.model.downloads_by_date():
            for version in data["flatpak_versions"]:
                if version not in self.versions:
                    self.versions.append(version)

        self.versions.append("unknown")

    @property
    def axis(self):
        return ["Time", "Downloads"]

    def data_zipped(self):
        return [
            zip(
                *(
                    [
                        date,
                        self.unknown_count(data) if version == "unknown" else data["flatpak_versions"].get(version, 0)
                    ]
                    for date, data in self.model.downloads_by_date()
                )
            ) for version in self.versions
        ]

    def data_zipped_kwargs(self):
        return [
            {
                "label": version,
                "x_date": True,
            } for version in self.versions
        ]

    @property
    def title(self):
        return (
            "Downloads by flatpak version in %s over time - %s" % (
                self.model.api.NAME,
                datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S")
            )
        )

    def unknown_count(self, data):
        return data["downloads"] - sum(data["flatpak_versions"].get(version, 0) for version in self.versions)
