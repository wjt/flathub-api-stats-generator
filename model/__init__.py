"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class BaseModel:
    NAME = "BaseModel"

    def __init__(self, api, extra={}):
        self.api = api
        self.api.check(extra)  # checks API is setup (eg Tokens)

    def __len__(self):
        return 0

    def load(self):
        for _ in self.load_generator():
            pass

    def load_generator(self):
        return []


from .app import AppModel
from .download import DownloadModel
