"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class DatWriter:
    @staticmethod
    def from_report(filename, report):
        if report.HAS_DATA_STRING:
            with open(filename, "w") as f:
                f.writelines(map(lambda a: "%s\n" % a,
                                 report.data_string()))

            print("Generated '%s'" % filename)
        else:
            print("Report does not support dat output")
