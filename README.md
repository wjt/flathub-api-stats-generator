This is a basic tool which is able to query the following APIs, to generate data and graphs about apps within [Flathub](http://flathub.org/).

  * https://api.github.com/
  * https://flathub.org/api/v1/

A basic example could be the following, use `--help` to see further options.

```
python3 main.py -o output.png
xdg-open output.png
```

One can also specify which API to use, different report modes, whether to generate a data file or graph.

```
python3 main.py --api flathub --report list_by_name --output output.dat --type data
```

Or a more complex example which performs multiple reports at the same could be the following

```
python3 main.py --report count_by_time downloads downloads_by_arch downloads_by_flatpak_version downloads_by_ostree_version --type graph --output a.png b.png c.png d.png e.png
```

Here is the example output of apps in Flathub over time.

![Flathub Stats](http://ahayzen.com/direct/flathub.png)

# Rational

I have been running a cronjob hourly on a local machine which runs `flatpak remote-ls --app flathub | wc -l`, checks if the count has changed from the previous hour, writes to a data file and then finally generates a PNG graph. This PNG is then a nice graph showing the growth of flatpak applications in flathub over time.

This works well, however it requires my local machine to be running to update the statistics. Now I could just install `flatpak` onto a server, however it pulls in a lot of dependencies that aren't relevant for a server environment.

Therefore I decided to create a simple tool which queries available APIs to find out the same information (and kept the code vaguely flexibile to doing further reports :-) ).

This then has the advantage of being able to run on a server without many dependencies (just python3, python3-matplotlib and their dependencies) and it allows for generating the graph back to zero - before I was missing data.

# APIs

Flathub is the default API which is used, but GitHub can also be used with personal tokens (this is useful for comparing any apps that are missing from the store).

## Tokens

When using the GitHub API a guest user is limited to 50 requests per day, one can [generate a token](https://github.com/settings/tokens) which allows many more :-)

This is then passed to the python script either with `---token` or by placing the token inside a file named `.github-token` in the run directory.

# Usage

The python script can be used with arguments. There are options for which API, output filenames, API token, report modes, data output type (graph or space delimited file).

Use `python3 main.py --help` to find out how to use these.
