"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# We want to be headless
import matplotlib
matplotlib.use("Agg")

import matplotlib.pyplot as plt


class Graph:
    @staticmethod
    def from_report(filename, report):
        if report.HAS_DATA_ZIPPED:
            Graph.new()

            kwargs = report.data_zipped_kwargs()

            if len(kwargs) > 0 and ("x_date" in kwargs[0] or "y_date" in kwargs[0]):
                method = Graph.set_data_with_dates
            else:
                method = Graph.set_data

            for extra, data_series in zip(kwargs, report.data_zipped()):
                method(*data_series, **extra)

            Graph.set_axis(*report.axis)
            Graph.set_title(report.title)

            Graph.save(filename)
        else:
            print("Report (%s) does not support graph output" % report)

    @staticmethod
    def new():
        plt.clf()
        plt.figure(figsize=(10, 10), dpi=100)

    @staticmethod
    def save(filename):
        # Add a margin to the x-axis so we can see the start
        # and end points clearly
        plt.margins(x=0.025, y=0.025, tight=False)

        # Ensure we don't show negative in the y-axis
        plt.ylim(ymin=0)

        plt.savefig(filename)

    @staticmethod
    def set_axis(x_label, y_label):
        plt.xlabel(x_label)
        plt.ylabel(y_label)

    @staticmethod
    def set_data(x_series, y_series, **extra):
        plt.plot(x_series, y_series, **extra)

        if "label" in extra:
            plt.legend(loc=2)  # loc is upper left

    @staticmethod
    def set_data_with_dates(x_series, y_series, x_date=False, y_date=False,
                            **extra):
        if x_date:
            plt.xticks(rotation=30)

        if y_date:
            plt.yticks(rotation=30)

        plt.plot_date(x_series, y_series, "-", xdate=x_date, ydate=y_date,
                      **extra)

        if "label" in extra:
            plt.legend(loc=2)  # loc is upper left

    @staticmethod
    def set_title(title):
        plt.title(title)
